import { Component, OnInit } from '@angular/core';
import { Teacher } from './teacher';
import { TeacherService } from './teacher.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public teachers: Teacher[] | undefined;
  public editTeacher: Teacher | undefined;
  public deleteTeacher: Teacher | undefined;

  constructor(private teacherService: TeacherService) {}

  ngOnInit() {
    this.getStudents();
  }

  public getStudents(): void {
    this.teacherService.getTeachers().subscribe(
      (response: Teacher[]) => {
        this.teachers = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      );
  }

  public onAddStudent(addForm: NgForm): void {
    document.getElementById('add-teacher-form')?.click();
    this.teacherService.addTeacher(addForm.value).subscribe(
      (response: Teacher) => {
        console.log(response);
        this.getStudents();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateStudent(teacher: Teacher): void {
    this.teacherService.updateTeacher(teacher).subscribe(
      (response: Teacher) => {
        console.log(response);
        this.getStudents();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteStudent(teacherId: number | undefined): void {
    this.teacherService.deleteTeacher(teacherId!).subscribe(
      (response: void) => {
        console.log(response);
        this.getStudents();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchStudents(key: string): void {
    const results: Teacher[] = [];
    for (const teacher of this.teachers!) {
      if (teacher.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || teacher.email.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || teacher.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(teacher);
      }
    }
    this.teachers = results;
    if (results.length === 0 || !key) {
      this.getStudents();
    }
  }

  public onOpenModal(mode: string, student?: Teacher):void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addStudentModal');
    }
    if (mode === 'edit') {
      this.editTeacher = student;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteTeacher = student;
      button.setAttribute('data-target', '#deleteStudentModal');
    }
    container?.appendChild(button);
    button.click();
  }

}
